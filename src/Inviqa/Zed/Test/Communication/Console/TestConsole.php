<?php

namespace Inviqa\Zed\Test\Communication\Console;

use Spryker\Zed\Kernel\Communication\Console\Console;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * @method \Inviqa\Zed\Test\Communication\TestCommunicationFactory getFactory()
 */
class TestConsole extends Console
{
    protected function configure()
    {
        $this->setName('inviqa:test');
        $this->setDescription('Testing Inviqa module');
        $this->addArgument('name', InputArgument::OPTIONAL);

        parent::configure();
    }
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln($this->getFactory()->getConfig()->getTestValue());
    }
}
