<?php

namespace Inviqa\Zed\Test\Communication;

use Spryker\Zed\Kernel\Communication\AbstractCommunicationFactory;

/**
 * @method \Inviqa\Zed\Test\TestConfig getConfig()
 */
class TestCommunicationFactory extends AbstractCommunicationFactory
{

}
