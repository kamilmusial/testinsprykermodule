<?php

namespace Inviqa\Zed\Test;

use Inviqa\Shared\Test\TestConstants;
use Spryker\Zed\Kernel\AbstractBundleConfig;

class TestConfig extends AbstractBundleConfig
{
    public function getTestValue()
    {
        return $this->get(TestConstants::TEST_CONSTANT, 'I\m not working...');
    }
}
