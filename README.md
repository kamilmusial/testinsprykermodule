# Inviqa Spryker Test Module

## Config:
 
* In `config_default.php`:
```php
$config[KernelConstants::PROJECT_NAMESPACES] = [
    'Inviqa',
    'Pyz',
];
```

```php
$config[TestConstants::TEST_CONSTANT] = 'I\'m working! :)';
```


* In `\Pyz\Zed\Console\ConsoleDependencyProvider::getConsoleCommands`

```php
new \Inviqa\Zed\Test\Communication\Console\TestConsole()
```